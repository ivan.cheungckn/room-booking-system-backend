import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as session from 'express-session';
import * as passport from 'passport';
import * as dotenv from 'dotenv';
dotenv.config();
async function bootstrap() {
    const app = await NestFactory.create(AppModule);

    app.enableCors({
        origin: true,
        credentials: true,
    });
    app.use(
        session({
            secret: `${process.env.SECRET}`,
            resave: false,
            saveUninitalized: false,
            cookie: { maxAge: 360000 },
        }),
    );
    app.use(passport.initialize());
    app.use(passport.session());
    await app.listen(8080);
}
bootstrap();
