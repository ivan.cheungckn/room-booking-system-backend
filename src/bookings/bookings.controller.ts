import {
    BadRequestException,
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Post,
    Query,
    UseGuards,
} from '@nestjs/common';
import { AuthenticatedGuard } from 'src/guards/authenticated.guard';
import { CurrentUser } from 'src/users/decorators/current-user.decorator';
import { User } from 'src/users/entities/users.entity';
import { BookingsService } from './bookings.service';
import { CreateBookingDto } from './dtos/create-booking.dto';
import { GetBookingDto } from './dtos/get-booking.dto';
import { RemoveBookingDto } from './dtos/remove-booking.dto';

@Controller('api/v1/bookings')
export class BookingsController {
    constructor(private booksService: BookingsService) {}

    @Post()
    @UseGuards(AuthenticatedGuard)
    async create(
        @Body()
        createBookingDto: CreateBookingDto,
        @CurrentUser() user: User,
    ) {
        return await this.booksService.create(createBookingDto, user);
    }

    @Get()
    async findAll(@Query() getBookingDto: GetBookingDto) {
        const startDate = getBookingDto.startDate;
        const endDate = getBookingDto.endDate;
        const result = await this.booksService.findByStartDateAndEndDate(
            startDate,
            endDate,
        );
        result.forEach((booking) => {
            delete booking.user.password;
        });
        return result;
    }

    @Get('my')
    @UseGuards(AuthenticatedGuard)
    async findByUser(@CurrentUser() user: User) {
        return await this.booksService.findByUserId(user.id);
    }

    @Delete(':bookingId')
    @UseGuards(AuthenticatedGuard)
    async remove(@Param('bookingId') bookingId, @CurrentUser() user: User) {
        return await this.booksService.remove(bookingId, user);
    }
}
