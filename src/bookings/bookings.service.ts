import {
    BadRequestException,
    Injectable,
    NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/users/entities/users.entity';
import { Repository } from 'typeorm';
import { RoomsService } from '../rooms/rooms.service';
import { CreateBookingDto } from './dtos/create-booking.dto';
import { Booking } from './entities/booking.entity';

@Injectable()
export class BookingsService {
    constructor(
        @InjectRepository(Booking) private repo: Repository<Booking>,
        private roomsService: RoomsService,
    ) {}

    findByStartDateAndEndDate(startDate: Date, endDate: Date) {
        return this.repo
            .createQueryBuilder('booking')
            .innerJoinAndSelect('booking.user', 'user')
            .innerJoinAndSelect('booking.room', 'room')
            .where(
                'booking.startDate >= :startDate and booking.endDate <= :endDate',
                { startDate, endDate },
            )
            .getMany();
    }

    findByUserId(userId: number) {
        return this.repo
            .createQueryBuilder('booking')
            .innerJoinAndSelect('booking.room', 'room')
            .where('booking.user = :userId', { userId })
            .orderBy('booking.startDate', 'DESC')
            .getMany();
    }

    findBookingsByUserIdAndBookingId(userId: number, bookingId: number) {
        return this.repo
            .createQueryBuilder('booking')
            .where('booking.id = :bookingId', { bookingId })
            .andWhere('booking.user = :userId', { userId })
            .getOne();
    }

    getCountAnyOverlappedBookings(
        startDate: Date,
        endDate: Date,
        userId: number,
    ) {
        return this.repo
            .createQueryBuilder('booking')
            .innerJoin('booking.user', 'user')
            .where(
                '(booking.startDate < :endDate) and (booking.endDate > :startDate)',
                { startDate, endDate },
            )
            .andWhere('user.id = :userId', { userId })
            .getMany();
    }

    findBookingsByRoomAndDate(roomId: number, startDate: Date, endDate: Date) {
        return this.repo
            .createQueryBuilder('booking')
            .where('booking.room = :roomId', { roomId })
            .andWhere(
                '(booking.startDate < :endDate) and (booking.endDate > :startDate)',
                { startDate, endDate },
            )
            .getMany();
    }

    async create(createBookingDto: CreateBookingDto, user: User) {
        const room = await this.roomsService.findOne(createBookingDto.room);
        if (!room) throw new NotFoundException('Room not found');
        const startDate = createBookingDto.startDate;
        const endDate = createBookingDto.endDate;
        if (!this.isHourDiff(startDate, endDate))
            throw new BadRequestException(
                "Start date and end date 's diff is not by hour",
            );
        if (
            !this.isDateMinSecMilliSecZero(startDate) ||
            !this.isDateMinSecMilliSecZero(endDate)
        )
            throw new BadRequestException(
                "StartDate or EndDate 's ms /s/m is not zero",
            );
        const existingBookings = await this.findBookingsByRoomAndDate(
            room.id,
            startDate,
            endDate,
        );
        if (existingBookings.length > 0)
            throw new BadRequestException('Room is booked by others');
        const overlappedCount = await this.getCountAnyOverlappedBookings(
            startDate,
            endDate,
            user.id,
        );
        if (overlappedCount.length > 0)
            throw new BadRequestException('There is overlapped booking');
        const booking = this.repo.create({ startDate, endDate, user, room });
        return this.repo.save(booking);
    }

    async remove(bookingId: number, user: User) {
        const booking = await this.findBookingsByUserIdAndBookingId(
            user.id,
            bookingId,
        );
        if (!booking) throw new BadRequestException('No bookings was booked.');
        if (new Date() > booking.startDate)
            throw new BadRequestException(
                'Can not delete booking since the booking is started or expired. ',
            );
        return await this.repo.remove(booking);
    }

    private isHourDiff(startDate: Date, endDate: Date) {
        const diff = (endDate.getTime() - startDate.getTime()) / 1000;
        return diff % 60 === 0;
    }
    private isDateMinSecMilliSecZero(date: Date) {
        if (date.getMilliseconds() !== 0) return false;
        if (date.getMinutes() !== 0) return false;
        if (date.getSeconds() !== 0) return false;
        return true;
    }
}
