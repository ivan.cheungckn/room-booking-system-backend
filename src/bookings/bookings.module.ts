import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BookingsController } from './bookings.controller';
import { BookingsService } from './bookings.service';
import { Booking } from './entities/booking.entity';
import { Room } from '../rooms/entities/room.entity';
import { RoomsService } from '../rooms/rooms.service';
import { RoomsModule } from 'src/rooms/rooms.module';

@Module({
    imports: [TypeOrmModule.forFeature([Booking]), RoomsModule],
    controllers: [BookingsController],
    providers: [BookingsService],
})
export class BookingsModule {}
