import { User } from 'src/users/entities/users.entity';
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    ManyToOne,
    Unique,
    Index,
} from 'typeorm';
import { Room } from '../../rooms/entities/room.entity';

@Entity()
export class Booking {
    @PrimaryGeneratedColumn()
    id: number;

    @Index()
    @Column()
    startDate: Date;

    @Index()
    @Column()
    endDate: Date;

    @ManyToOne(() => Room, (room) => room.bookings)
    room: Room;

    @ManyToOne(() => User, (user) => user.bookings)
    user: User;
}
