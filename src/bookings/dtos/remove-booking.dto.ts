import { Transform } from 'class-transformer';
import {
    IsDate,
    IsEmail,
    IsNotEmpty,
    IsPhoneNumber,
    IsString,
} from 'class-validator';

export class RemoveBookingDto {
    @IsString()
    @IsNotEmpty()
    bookingId: string;
}
