import { Transform } from 'class-transformer';
import {
    IsDate,
    IsEmail,
    IsNotEmpty,
    IsPhoneNumber,
    IsString,
} from 'class-validator';
import * as moment from 'moment';

export class CreateBookingDto {
    @IsString()
    @IsNotEmpty()
    room: string;

    @IsDate()
    @IsNotEmpty()
    @Transform(({ value }) => new Date(value))
    startDate: Date;

    @IsDate()
    @IsNotEmpty()
    @Transform(({ value }) => new Date(value))
    endDate: Date;
}
