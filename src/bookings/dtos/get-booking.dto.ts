import { Transform } from 'class-transformer';
import {
    IsDate,
    IsDateString,
    IsEmail,
    IsNotEmpty,
    IsPhoneNumber,
    IsString,
} from 'class-validator';
import * as moment from 'moment';

export class GetBookingDto {
    @IsDateString()
    @IsNotEmpty()
    startDate: Date;

    @IsDateString()
    @IsNotEmpty()
    endDate: Date;
}
