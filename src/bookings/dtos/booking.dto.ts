import { Expose } from 'class-transformer';
import { RoomDto } from 'src/rooms/dtos/room.dto';
import { UserDto } from 'src/users/dtos/user.dto';

export class BookingDto {
    @Expose()
    id: number;

    @Expose()
    startDate: string;

    @Expose()
    endDate: string;

    @Expose()
    user: UserDto;

    @Expose()
    room: RoomDto;
}
