import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    OneToMany,
    Index,
} from 'typeorm';
import { Booking } from '../../bookings/entities/booking.entity';

@Entity()
export class Room {
    @PrimaryGeneratedColumn()
    id: number;

    @Index()
    @Column({ unique: true })
    code: string;

    @OneToMany(() => Booking, (booking) => booking.room)
    bookings: Booking[];
}
