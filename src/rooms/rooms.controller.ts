import { Controller, Get } from '@nestjs/common';
import { RoomsService } from './rooms.service';

@Controller('api/v1/rooms')
export class RoomsController {
    constructor(private roomsService: RoomsService) {}

    @Get()
    async find() {
        return await this.roomsService.find();
    }
}
