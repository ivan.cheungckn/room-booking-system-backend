import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Room } from './entities/room.entity';

@Injectable()
export class RoomsService {
    constructor(@InjectRepository(Room) private repo: Repository<Room>) {}

    findOne(code: string) {
        return this.repo.findOne({ code });
    }

    find() {
        return this.repo.find();
    }
}
