import { Expose } from 'class-transformer';

import * as moment from 'moment';

export class RoomDto {
    @Expose()
    id: number;

    @Expose()
    code: string;
}
