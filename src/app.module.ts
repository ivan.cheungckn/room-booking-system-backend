import { Module, ValidationPipe } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/entities/users.entity';
import { APP_PIPE } from '@nestjs/core';
import { BookingsModule } from './bookings/bookings.module';
import { Room } from './rooms/entities/room.entity';
import { Booking } from './bookings/entities/booking.entity';
import { RoomsModule } from './rooms/rooms.module';
import * as dotenv from 'dotenv';
dotenv.config();

@Module({
    imports: [
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: `${process.env.DB_HOST}`,
            port: parseInt(`${process.env.DB_PORT}`),
            username: `${process.env.DB_USERNAME}`,
            password: `${process.env.DB_PASSWORD}`,
            database: `${process.env.DB_TABLE}`,
            entities: [User, Booking, Room],
            synchronize: true,
        }),
        UsersModule,
        BookingsModule,
        RoomsModule,
    ],
    providers: [
        {
            provide: APP_PIPE,
            useValue: new ValidationPipe({
                whitelist: true,
            }),
        },
    ],
})
export class AppModule {}
