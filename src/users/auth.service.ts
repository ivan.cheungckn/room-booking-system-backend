import { Injectable, NotFoundException } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
    constructor(private usersService: UsersService) {}
    async validateUser(username: string, password: string) {
        const user = await this.usersService.findOne(username);
        if (!user) throw new NotFoundException('User not found');
        if (await bcrypt.compare(password, user.password)) {
            return user;
        } else {
            return null;
        }
    }
}
