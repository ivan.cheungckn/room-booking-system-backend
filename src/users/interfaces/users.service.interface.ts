import { CreateUserDto } from '../dtos/create-user.dto';

export interface IUsersService {
    login();

    logout();

    register(createUserDto: CreateUserDto);
}
