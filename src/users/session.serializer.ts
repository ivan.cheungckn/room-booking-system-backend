import { PassportSerializer } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { UsersService } from './users.service';
import { User } from './entities/users.entity';

@Injectable()
export class SessionSerializer extends PassportSerializer {
    constructor(private usersService: UsersService) {
        super();
    }
    serializeUser(user: User, done: (err: Error, user: any) => void) {
        done(null, user.username);
    }
    async deserializeUser(
        payload: any,
        done: (err: Error, payload: any) => void,
    ) {
        //payload is the user.username
        const user = await this.usersService.findOne(payload);
        delete user.password;
        done(null, user);
    }
}
