import {
    Body,
    Controller,
    Get,
    HttpCode,
    Post,
    Req,
    UseGuards,
} from '@nestjs/common';
import { AuthenticatedGuard } from 'src/guards/authenticated.guard';
import { LocalAuthGuard } from 'src/guards/local-auth.guard';
import { Serialize } from 'src/interceptors/serialize.interceptor';
import { CurrentUser } from './decorators/current-user.decorator';
import { CreateUserDto } from './dtos/create-user.dto';
import { GeneralResponseDto } from './dtos/general.response.dto';
import { UserDto } from './dtos/user.dto';
import { User } from './entities/users.entity';
import { UsersService } from './users.service';
import { Request } from 'express';
@Controller('api/v1/users')
export class UsersController {
    constructor(private usersService: UsersService) {}

    @Post('login')
    @UseGuards(LocalAuthGuard)
    @Serialize(UserDto)
    @HttpCode(200)
    login(@CurrentUser() user: User) {
        return user;
    }

    @Post('logout')
    @HttpCode(200)
    logout(@Req() request: Request) {
        request.logout();
        const response: GeneralResponseDto = new GeneralResponseDto();
        response.success = true;
        return response;
    }

    @Post('register')
    @Serialize(UserDto)
    @HttpCode(201)
    register(@Body() body: CreateUserDto) {
        return this.usersService.register(body);
    }

    @Get('current')
    @UseGuards(AuthenticatedGuard)
    @Serialize(UserDto)
    whoami(@CurrentUser() user: User) {
        return user;
    }

    @Get()
    @UseGuards(AuthenticatedGuard)
    test(@Req() request: Request) {
        return 'test';
    }
}
