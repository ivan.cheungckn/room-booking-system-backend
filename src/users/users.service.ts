import { BadRequestException, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dtos/create-user.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entities/users.entity';
import { company } from './enums/company.enum';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
    constructor(@InjectRepository(User) private repo: Repository<User>) {}

    async register(createUserDto: CreateUserDto) {
        const users = await this.find(createUserDto.username);
        if (users.length) {
            throw new BadRequestException(
                'username (staff Id) in use please use correct staff Id as username',
            );
        }

        const salt = await bcrypt.genSalt();
        const hashPassword = await bcrypt.hash(createUserDto.password, salt);

        // Create a new user and save it
        const company = this.isColaOrPepsiCompany(createUserDto.username);
        const user = await this.create(
            createUserDto.username,
            hashPassword,
            createUserDto.phone,
            createUserDto.name,
            company,
        );

        // return the user
        return user;
    }

    isColaOrPepsiCompany(username: string) {
        if (username.startsWith('C')) {
            return company.COLA;
        } else if (username.startsWith('P')) {
            return company.PEPSI;
        } else {
            throw new BadRequestException(
                'Invalid username please use correct staff Id as username',
            );
        }
    }

    find(username: string) {
        return this.repo.find({ username });
    }

    findOne(username: string) {
        return this.repo.findOne({ username });
    }

    create(
        username: string,
        password: string,
        phone: string,
        name: string,
        company: string,
    ) {
        const user = this.repo.create({
            username,
            password,
            name,
            phone,
            company,
        });

        return this.repo.save(user);
    }
}
