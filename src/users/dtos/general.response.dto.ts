import { Expose } from 'class-transformer';

export class GeneralResponseDto {
    @Expose()
    success: boolean;
}
