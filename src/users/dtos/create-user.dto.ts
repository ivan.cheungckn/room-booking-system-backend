import { Transform } from 'class-transformer';
import { IsEmail, IsPhoneNumber, IsString } from 'class-validator';

export class CreateUserDto {
    @IsString()
    username: string;

    @IsString()
    name: string;

    @IsPhoneNumber('HK')
    phone: string;

    @IsString()
    password: string;
}
