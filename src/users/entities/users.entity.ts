import { Exclude } from 'class-transformer';
import { Booking } from 'src/bookings/entities/booking.entity';
import {
    Entity,
    Column,
    PrimaryGeneratedColumn,
    OneToMany,
    Index,
} from 'typeorm';

@Entity()
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Index()
    @Column({ unique: true })
    username: string;

    @Column()
    @Exclude()
    password: string;

    @Column()
    phone: string;

    @Column()
    name: string;

    @Column()
    company: string;

    @OneToMany(() => Booking, (booking) => booking.user)
    bookings: Booking[];
}
